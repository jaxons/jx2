//= require jquery/dist/jquery.min.js
//= require fullpage.js/vendors/jquery.easings.min.js
//= require jquery.slimscroll/jquery.slimscroll.min.js
//= require fullpage.js/jquery.fullPage.min.js
//= require_tree .

$(document).ready(function() {
  $('#fullpage').fullpage({
    anchors: ['home', 'detachering', 'contact'],
    menu: '#menu',
    easing: 'easeOutQuad'  });

  $('#test').hide();
});
